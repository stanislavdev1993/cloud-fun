<?php

namespace common\helpers;

use common\models\File;

class JsHelper
{
    public static function jsWarning($message)
    {
        $message = addslashes($message);
        return "console.warn('$message');\n";
    }

    public static function insertStyleCode($id, $src)
    {
        $type = File::TYPE_STYLE;
        return "CFcore.getComponent('Inserter').register({id: $id, src: '$src', type: $type});\n";
    }

    public static function import($componentId, $key, $value)
    {
        return "CFcore.importRegister($componentId, '$key', `$value`);\n";
    }

    public static function registerComponentAlias($id, $alias)
    {
        return "CFcore.registerComponentAlias('CFComponent$id', '$alias');\n";
    }

    public static function registerParams($id, $params)
    {
        return "CFcore.registerParams('CFComponent$id', '$params');\n";
    }
}