<?php
namespace common\helpers;

use Yii;

class FileHelper
{
    public static function createUserFolder($folder)
    {
        $path = Yii::getAlias("@data/$folder");
        $mkdir =  mkdir($path);
        return $mkdir && chmod($path, 0777);
    }

    public static function base64ToFile($fileName)
    {
        $base64_string = file_get_contents("$fileName-tmp");
        // open the output file for writing
        $ifp = fopen( $fileName, 'wb' );

        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $base64_string );

        // we could add validation here with ensuring count( $data ) > 1
        fwrite( $ifp, base64_decode( $data[ 1 ] ) );

        // clean up the file resource
        fclose( $ifp );
        chmod($fileName, 0777);

        unlink("$fileName-tmp");
        return $fileName;
    }
}