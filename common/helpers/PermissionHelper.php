<?php
namespace common\helpers;

use yii\helpers\ArrayHelper;

class PermissionHelper
{
    public static function generateForOwner()
    {
        return [
            'owner' => [
                'user_id' => \Yii::$app->user->getId(),
                'access' => ['read', 'write']
            ],
            'group' => [
                'group_id' => 0,
                'access' => []
            ],
            'other' => [
                'access' => []
            ]
        ];
    }

    public static function canRead($permission, $userId)
    {
        return self::can($permission, $userId, 'read');
    }

    public static function canWrite($permission, $userId)
    {
        return self::can($permission, $userId, 'write');
    }

    protected static function can($permission, $userId, $action)
    {
        if (!\Yii::$app->user->isGuest) {
            $groups = ArrayHelper::map(\Yii::$app->user->identity->groups, 'title', 'id');
        } else {
            $groups = [];
        }

        if ($permission->owner->user_id === $userId
            && in_array($action, $permission->owner->access)) {
            return true;
        } elseif (in_array($permission->group->group_id, $groups)
            && in_array($action, $permission->group->access)) {
            return true;
        } elseif ($userId === null
            && in_array($action, $permission->other->access)) {
            return true;
        }

        return false;
    }
}