<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Conformity extends ActiveRecord
{
    public function behaviors()
    {
        return [TimestampBehavior::className()];
    }

    public function rules()
    {
        return [
            ['user_id', 'component_id', 'integer'],
            ['mime_type', 'string', 'max' => 10]
        ];
    }
}