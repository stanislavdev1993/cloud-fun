<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "installs".
 *
 * @property integer $id
 * @property string $slug
 * @property integer $status
 * @property integer $file_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Files $file
 */
class Group extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'string']
        ];
    }
}