<?php

namespace common\models;

use MongoDB\BSON\Timestamp;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Storage extends ActiveRecord
{
    public function behaviors()
    {
        return [TimestampBehavior::className()];
    }
    public static function tableName()
    {
        return 'storage';
    }

    public function rules()
    {
        return [
            ['user_id', 'integer'],
            ['key', 'string', 'max' => 255],
            ['value', 'string'],
            ['user_id', 'unique', 'targetAttribute' => ['key']]
        ];
    }
}