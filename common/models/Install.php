<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "installs".
 *
 * @property integer $id
 * @property string $slug
 * @property integer $status
 * @property integer $file_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Files $file
 */
class Install extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'installs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'file_id', 'created_at', 'updated_at'], 'integer'],
            [['slug'], 'string', 'max' => 255],
            [['file_id'], 'exist', 'skipOnError' => true, 'targetClass' => Files::className(), 'targetAttribute' => ['file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slug' => 'Slug',
            'status' => 'Status',
            'file_id' => 'File ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'file_id']);
    }
}