<?php

namespace common\models;

use common\components\Loader;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "autoload".
 *
 * @property integer $user_id
 * @property integer $file_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class Autoload extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'autoload';
    }
    public function attributes()
    {
        $attributes = parent::attributes(); // TODO: Change the autogenerated stub
        $attributes[] = 'src';
        $attributes[] = 'type';

        return $attributes;
    }

    public function afterFind()
    {
        $this->setAttribute('src', 'http://api.cloud-fun.loc/loader/load?id=' . $this->file_id);
        $type = $this->file->type;
        $this->setAttribute('type', $type);
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['src', 'string'],
            [['user_id', 'file_id', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'file_id' => 'File ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'file_id']);
    }
}