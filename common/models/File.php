<?php

namespace common\models;

use common\components\Loader;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "files".
 *
 * @property integer $id
 * @property string $src
 * @property string $permission
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $type
 */
class File extends \yii\db\ActiveRecord
{
    const MODE_REWRITE = 1;
    const MODE_APPEND = 2;
    const TYPE_SCRIPT = 1;
    const TYPE_STYLE = 2;
    const TYPE_FILE = 3;
    const TYPE_COMPONENT = 4;

    public $buildSource;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

    public function behaviors()
    {
        return [TimestampBehavior::className()];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'type'], 'integer'],
            [['src', 'permission', 'alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'src' => 'Src',
            'permission' => 'Permission',
            'status' => 'Status',
            'type' => 'Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}