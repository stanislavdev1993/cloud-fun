<?php

namespace common\components;

use common\helpers\JsHelper;
use common\helpers\PermissionHelper;
use common\models\File;
use common\models\User;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class Loader extends Component
{

    public static $types = [
        'css' => 1,
        'js' => 2
    ];

    /**
     * @param $id integer|string id or slug
     * @param  $type integer program or file
     * @param $params array
     */
    public function load($id, $params)
    {
        $model = null;

        $message = 'File not found';
        if (is_numeric($id)) {
            $model = File::findOne($id);
        } else {
            $model = File::findOne(['alias' => $id]);
        }

        if ($model != null) {
            $userId = \Yii::$app->user->isGuest ? null : \Yii::$app->user->getId();
            $permission = json_decode($model->permission);

            if (!PermissionHelper::canRead($permission, $userId)) {
                throw new ForbiddenHttpException('You don\'t have permission');
            }

            if ($model->type == FILE::TYPE_COMPONENT) {
                // build component js
                // try to get component config

                $componentDir = dirname($model->src);
                $configFile = $componentDir . '/config.json';
                if (file_exists(\Yii::getAlias($configFile))) {
                    $config = json_decode(file_get_contents(\Yii::getAlias($configFile)), true);

                    $requires = '';
                    $imports = '';
                    $aliases = '';

                    if (!empty($params)) {
                        $params = "\n;" . JsHelper::registerParams($config['componentId'], $params);
                    }
                    if (!empty($model->alias)) {
                        $aliases = JsHelper::registerComponentAlias($model->id, $model->alias);
                    }
                    if (isset($config['imports'])) {
                        $imports = $this->getImports($config['componentId'],
                            $config['imports'],
                            $componentDir
                        );
                    }

                    if (isset($config['requireStyles'])) {
                        $requires .= $this->getRequires($config['componentId'], $config['requireStyles'], $componentDir);
                    }
                    if (isset($config['requireScripts'])) {
                        $requires .= $this->getRequires($config['componentId'], $config['requireScripts'], $componentDir);

                        // append main file
                        $requires .= file_get_contents(\Yii::getAlias($model->src));
                    }

                    $requires = $aliases . $imports . $requires . $params;

                    if (!empty($requires)) {
                        $model->buildSource = $requires;
                    }
                }
            }

            return $model;

        }

        throw new NotFoundHttpException($message, 404);
    }

    protected function getImports($componentId, $imports, $componentDir)
    {
        $result = '';

        foreach ($imports as $key => $value) {
            $file = File::findOne(['src' => "$componentDir/$value"]);

            if ($file != null && PermissionHelper::canRead(json_decode($file->permission), 1)) {
                $result .= JsHelper::import($componentId, $key, $this->getFile($file->id, \Yii::getAlias($file->src), $componentId));
            } else {
                $result .= JsHelper::jsWarning("Don't have permission to $value");
            }
        }

        return $result;
    }
    protected function getRequires($componentId, $requires, $basePath)
    {
        $result = '';

        foreach ($requires as $require) {
            if (file_exists($file = \Yii::getAlias("$basePath/$require"))) {
                // try find file in db and check permission
                $file = File::findOne(['src' => "$basePath/$require"]);

                if ($file != null && PermissionHelper::canRead(json_decode($file->permission), 1)) {
                    $result .= $this->getFile($file->id, \Yii::getAlias($file->src), $componentId);
                } else {
                    $result .= JsHelper::jsWarning("Don't have permission to $require");
                }
            } else {
                // throw JS warning
                $result .= JsHelper::jsWarning("File $require not founded");
            }
        }

        return $result;
    }

    protected function getFile($id, $src, $componentId)
    {
        $ext = pathinfo($src, PATHINFO_EXTENSION);
        if ($ext == 'js') {
            return $this->getScript($id, $src, $componentId);
        } elseif ($ext == 'css') {
            return $this->getStyle($id, $componentId);
        } else {
            return file_get_contents($src);
        }
    }

    protected function getFileUrl($id)
    {
        return Url::base(true) . '/loader/load?id=' . $id;
    }
    protected function getScript($id, $src, $componentId)
    {
        return file_get_contents($src);
    }

    protected function getStyle($id, $componentId)
    {
        return JsHelper::insertStyleCode($componentId, $this->getFileUrl($id));
    }
}