<?php
namespace common\components;

use common\helpers\PermissionHelper;
use common\models\Conformity;
use common\models\File;

class DbUploadHandler extends UploadHandler
{
    public function __construct($options = null, $initialize = true, $error_messages = null)
    {
        parent::__construct($options, $initialize, $error_messages);
    }
    protected function set_additional_file_properties($file) {
        parent::set_additional_file_properties($file);
        $this->saveToDb($file);
    }

    protected function saveToDb($file) {
        $fileObj = new File();

        $basePath = $this->options['upload_dir'];
        $url = $this->options['upload_url'];
        $fileObj->src = "$basePath/$file->name";
        $fileObj->status = 1;
        $fileObj->type = File::TYPE_FILE;
        $fileObj->permission = json_encode(PermissionHelper::generateForOwner());

        $conformity = Conformity::find()->where([
            'mime_type' => $file->type,
            'user_id' => 0
        ])->orWhere([ // find core components assosiations
            'mime_type' => $file->type,
            'user_id' => \Yii::$app->user->getId()
        ])->orderBy('user_id DESC')->one();

        $componentId = null;

        if ($conformity != null) {
            $componentId = $conformity->component_id;
        }

        if ($fileObj->save()) {
            $file->url = "$url?id={$fileObj->id}";
            $file->file_id = $fileObj->id;
            $file->component_id = $componentId;
        }

        chmod("$basePath{$file->name}", 0777);
    }
}