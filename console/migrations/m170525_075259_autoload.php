<?php

use yii\db\Migration;

class m170525_075259_autoload extends Migration
{
    public function up()
    {
        $this->createTable('{{%autoload}}', [
            'user_id' => $this->integer(),
            'file_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%autoload}}');
    }
}
