<?php

use yii\db\Migration;

class m170531_081807_add_file_type extends Migration
{
    public function up()
    {
        $this->addColumn('{{%files}}', 'type', $this->smallInteger(1));
    }

    public function down()
    {
        $this->dropColumn('{{%files}}', 'type');
    }
}
