<?php

use yii\db\Migration;

class m170703_092319_component_file extends Migration
{
    public function up()
    {
        $this->createTable('{{%component_file}}', [
            'name' => $this->string()->unique(),
            'file_id' => $this->string(),
            'user_id' => $this->integer(),
            'status' => $this->smallInteger(2),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%component_file}}');
    }
}
