<?php

use yii\db\Migration;

class m170525_122238_add_access_token extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'access_token', $this->string(32));
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'access_token');
    }
}
