<?php

use yii\db\Migration;

class m170703_103404_add_file_alias extends Migration
{
    public function up()
    {
        $this->addColumn('{{%files}}',
            'alias',
            $this->string()->unique()
        );
    }

    public function down()
    {
        $this->dropColumn('{{%files}}', 'alias');
    }
}
