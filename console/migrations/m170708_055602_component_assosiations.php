<?php

use yii\db\Migration;

class m170708_055602_component_assosiations extends Migration
{
    public function up()
    {
        $this->createTable('{{%conformity}}', [
            'id' => $this->primaryKey(),
            'component_id' => $this->integer(),
            'extension' => $this->string(10),
            'user_id' => $this->integer()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%conformity}}');
    }
}
