<?php

use yii\db\Migration;

class m170627_110902_add_user_folder extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}',
            'dir',
            $this->string(60)
        );
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'dir');
    }
}
