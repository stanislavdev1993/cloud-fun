<?php

use yii\db\Migration;

class m170525_143758_add_for_logined extends Migration
{
    public function up()
    {
        $this->addColumn('{{%autoload}}', 'for_guest',
            $this->smallInteger(1));
    }

    public function down()
    {
        $this->dropColumn('{{%autoload}}', 'for_guest');
    }
}
