<?php

use yii\db\Migration;

class m170531_080346_remove_installs_table extends Migration
{
    public function up()
    {
//        $this->dropIndex('install_file', '{{%installs}}');
        $this->dropTable('{{%installs}}');
    }

    public function down()
    {
        $this->createTable('{{installs}}', [
            'id' => $this->primaryKey(),
            'slug' => $this->string(255),
            'status' => $this->smallInteger(2),
            'file_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey('install_to_file', '{{%installs}}', 'file_id', '{{%files}}', 'id');
    }
}
