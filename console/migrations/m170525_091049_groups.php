<?php

use yii\db\Migration;

class m170525_091049_groups extends Migration
{
    public function up()
    {
        $this->createTable('{{%groups}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(50)->unique(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->createTable('{{%user_group}}', [
            'user_id' => $this->integer(),
            'group_id' => $this->integer()
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%groups}}');
        $this->dropTable('{{%user_group}}');
    }
}
