<?php

use yii\db\Migration;

class m170708_061211_rename_extension_to_mimetype extends Migration
{
    public function up()
    {
        $this->renameColumn('{{%conformity}}',
            'extension',
            'mime_type');
    }

    public function down()
    {
        $this->renameColumn('{{%conformity}}',
            'mime_type',
            'extension');
    }
}
