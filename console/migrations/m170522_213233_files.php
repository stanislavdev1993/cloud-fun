<?php

use yii\db\Migration;

class m170522_213233_files extends Migration
{
    public function up()
    {
        $this->createTable('{{files}}', [
            'id' => $this->primaryKey(),
            'src' => $this->string(255),
            'permission' => $this->string(255),
            'status' => $this->smallInteger(2),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    public function down()
    {
        $this->dropTable('{{files}}');
    }
}
