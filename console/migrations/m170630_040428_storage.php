<?php

use yii\db\Migration;

class m170630_040428_storage extends Migration
{
    public function up()
    {
        $this->createTable('{{%storage}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'key' => $this->string(),
            'value' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%storage}}');
    }
}
