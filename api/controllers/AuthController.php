<?php
namespace api\controllers;

use common\models\LoginForm;
use common\models\User;
use frontend\models\SignupForm;
use yii\rest\Controller;

class AuthController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        // For cross-domain AJAX request
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                // restrict access to domains:
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age' => 3600, // Cache (seconds)
            ]
        ];

        return $behaviors;
    }

    public function actionSignIn()
    {
        $loginForm = new LoginForm();

        if ($loginForm->load(\Yii::$app->request->post()) && $loginForm->login()) {
            return ['status' => 1,
                'access_token' => \Yii::$app->user->identity->access_token
            ];
        }

        return ['status' => 0, 'msg' => 'Wrong username or password'];
    }

    public function actionSignUp()
    {
        $signupForm = new SignupForm();

        if ($signupForm->load(\Yii::$app->request->post()) && $user = $signupForm->signup()) {
            return ['status' => 1, 'access_token' => $user->access_token];
        }

        return ['status' => 0, 'errors' => $signupForm->getErrors()];
    }
}
