<?php
namespace api\controllers;

use common\components\DbUploadHandler;
use \common\helpers\FileHelper;
use common\helpers\PermissionHelper;
use common\models\File;
use Yii;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\Cors;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\Response;

class FileController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        // For cross-domain AJAX request
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                // restrict access to domains:
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age' => 3600, // Cache (seconds)
            ]
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                // allow authenticated users
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
                // everything else is denied
            ],
        ];

        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'get' => ['get'],
                'write'  => ['post']
            ]
        ];

        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className()
        ];

        return $behaviors;
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    public function actionUpload()
    {
        $userDir = Yii::$app->user->identity->dir;
        $path = Yii::$app->request->post('path');
        $basePath = Yii::getAlias("@data/$userDir/$path/");
        $uploadHandler = new DbUploadHandler([
            'upload_dir' => $basePath,
            'base_path_alias' => "@data/$userDir/$path/",
            'mkdir_mode' => 0777,
            'upload_url' => 'http://api.cloud-fun.loc/loader/load',
            'image_versions' => []
        ]);
    }

    public function actionWrite()
    {
        $id = Yii::$app->request->post('id');
        $content = Yii::$app->request->post('content');
        $mode = (int)Yii::$app->request->post('mode');
        $userId = Yii::$app->user->getId();

        $file = File::findOne($id);

        if ($file != null) {
            if (PermissionHelper::canWrite(json_decode($file->permission), $userId)) {
                $src = Yii::getAlias($file->src);

                if ($mode == File::MODE_REWRITE) {
                    $result = file_put_contents($src, $content);
                } elseif ($mode == File::MODE_APPEND) {
                    $fileContent = file_get_contents($src);
                    $result = file_put_contents($src, $fileContent . $content);
                } else {
                    return ['status' => 0, "msg" => "Unknown mode"];
                }

                if ($result) return ['status' => 1, "msg" => $content];
            }
        }

        return ['status' => 0, 'msg' => "File $id not found"];
    }
}