<?php
namespace api\controllers;

use common\models\File;
use Yii;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\Cors;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;

class ComponentController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        // For cross-domain AJAX request
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                // restrict access to domains:
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age' => 3600, // Cache (seconds)
            ]
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                // allow authenticated users
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
                // everything else is denied
            ],
        ];

        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'icon' => ['get'],
            ]
        ];

        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className()
        ];

        return $behaviors;
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIcon($componentId)
    {
        // find component

        $component = File::findOne([
            'id' => $componentId,
            'type' => File::TYPE_COMPONENT
        ]);

        if ($component != null) {
            $path = dirname(Yii::getAlias($component->src));
            $config = json_decode(file_get_contents("$path/config.json"), true);

            if (isset($config['icon'])) {
                $icon = "$path/{$config['icon']}";
            } else {
                $icon = Yii::getAlias("@frontend/web/img/test.png");
            }

            return Yii::$app->response->sendFile($icon);
        }

        throw new NotFoundHttpException("Component icon not found");
    }
}