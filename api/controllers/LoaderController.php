<?php
namespace api\controllers;

use common\components\Loader;
use Yii;
use yii\filters\auth\QueryParamAuth;
use yii\rest\Controller;

class LoaderController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        // For cross-domain AJAX request
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                // restrict access to domains:
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age' => 3600, // Cache (seconds)
            ]
        ];

        if (!Yii::$app->request->get('access-token') == null) {
            $behaviors['authenticator'] = [
                'class' => QueryParamAuth::className()
            ];
        }

        return $behaviors;
    }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->redirect('/api/load?id=1&type=1');
    }
    /**
     * @return Loader
     */
    protected function getLoader()
    {
        return Yii::$app->loader;
    }

    /**
     * @param $params array
     * @param $id integer|string file_id or program id
     * @param $type integer is load type program|file
     */
    public function actionLoad($id, $params = '')
    {
        $loader = $this->getLoader();

        $model = $loader->load($id, $params);

        if (!empty($model->buildSource)) {
            return Yii::$app->response->sendContentAsFile($model->buildSource, basename($model->src), [
                'content-type' => 'text/javascript'
            ]);
        }

        return Yii::$app->response
            ->sendFile(Yii::getAlias($model->src), basename($model->src));
    }
}
