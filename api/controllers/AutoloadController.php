<?php

namespace api\controllers;

use common\models\Autoload;
use Yii;
use yii\filters\auth\QueryParamAuth;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class AutoloadController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        // For cross-domain AJAX request
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                // restrict access to domains:
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age' => 3600,                 // Cache (seconds)
            ]
        ];

        // it's not guest try to login
        if (!Yii::$app->request->get('access-token') == null) {
            $behaviors['authenticator'] = [
                'class' => QueryParamAuth::className()
            ];
        }

        return $behaviors;
    }
}
