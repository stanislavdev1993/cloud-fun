<?php
namespace api\controllers;

use common\models\Storage;
use Yii;
use yii\filters\auth\QueryParamAuth;
use yii\filters\Cors;
use \yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\rest\Controller;

class StorageController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        // For cross-domain AJAX request
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                // restrict access to domains:
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age' => 3600, // Cache (seconds)
            ]
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                // allow authenticated users
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
                // everything else is denied
            ],
        ];

        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'get' => ['get'],
                'set'  => ['post'],
                'update' => ['post']
            ]
        ];

        $behaviors['authenticator'] = [
                'class' => QueryParamAuth::className()
        ];

        return $behaviors;
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionGet($key)
    {
        if (!Yii::$app->user->isGuest) {
            $storage = Storage::findOne([
                'key' => $key,
                'user_id' => Yii::$app->user->getId()
            ]);

            return $storage ? $storage : null;
        }
    }

    public function actionSet()
    {
        $storage = new Storage();

        $storage->key = Yii::$app->request->post('key');
        $storage->value = Yii::$app->request->post('key');
        $storage->user_id = Yii::$app->user->getId();

        if ($storage->save()) {
            return ['status' => 1];
        }

        return ['status' => 0, 'errors' => $storage->getErrors()];
    }

    public function actionUpdate()
    {
        $key = Yii::$app->request->post('key');
        $value = Yii::$app->request->post('value');

        $storage = Storage::findOne([
            'key' => $key,
            'user_id' => Yii::$app->user->getId()
        ]);

        if ($storage != null) {
            if ($storage->updateAttributes(["value" => json_encode([
                'items' => $value
            ])])) {
                return ['status' => 1, 'msg' => 'Saved'];
            } else {
                return empty($storage->getErrors()) ?
                    ['status' => 0, 'msg' => 'Changes not found'] :
                    ['status' => 0, 'msg' => $storage->getErrors()];
            }
        }

        return ['status' => 0, 'msg' => "Key $key not found"];
    }
}
