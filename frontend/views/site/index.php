<?php

/* @var $this yii\web\View */

$this->title = 'Cloud fun';
?>

<div id="apps-container">
    <div id="welcome-container">
        <h1 class="cf-title">Cloud fun
            <i class="fa fa-spin fa-cog"></i>
        </h1>
    </div>
</div>
