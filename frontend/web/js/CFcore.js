// main core script
(function (window, document, CFcore) {
  "use strict";
  function getComponentSrc (id) {
    return "http://api.cloud-fun.loc/loader/load?id="+id;
  }
  function extend(child, parent)
  {
    var F = function () { };
    F.prototype = parent.prototype;

    child.prototype = new F();
    child.prototype.constructor = child;
    child.super = parent.prototype;
  }

  CFcore.appsContainer = document.getElementById('apps-container');
  CFcore.accessToken = null;
  CFcore.logined = false;
  CFcore.imports = {};
  CFcore.componentAliases = {};
  CFcore.components = {};

  CFcore.get = function (componentId) {
    return this.getComponent(componentId);
  };

  CFcore.registerParams = function (id, params) {
    this.components[id].params = JSON.parse(params);
  };
  CFcore.getParams = function (id) {
    return this.components[id].params;
  };
  CFcore.componentLoaded = function (script) {
    var componentId = script.getAttribute('data-id');
    var src = script.getAttribute('src');
    var host = document.createElement('div');
    var unique = new Date().valueOf();
    host.setAttribute(componentId, '');
    host.setAttribute("data-unique", unique);
    console.log(unique);


    if (!this.components[componentId]) {
      // component not found try get alias
      componentId = this.getComponentAlias(componentId);
      host.setAttribute(componentId, '');
    }

    this.components[componentId]['callback'](host, componentId, src);

    if (!this.components[componentId]['component']['host']) {
      console.log('Second step');
      this.components[componentId]['component']['id'] = componentId;
      this.components[componentId]['component']['unique'] = unique;
      console.log(this.components[componentId]["component"]);
      if (this.components[componentId]["component"] instanceof Component) {
        this.components[componentId]['component']['host'] = host;
      }
    } else {
      // it's child
      if (this.components[componentId]['component'].allowChild) {
        var index = this.components[componentId]['component'].children.length - 1;

        this.components[componentId]['component']['children'][index]['id'] = componentId;
        this.components[componentId]['component']['children'][index]['unique'] = unique;
        this.components[componentId]['component']['children'][index]['host'] = host;
      }
    }
  };

  CFcore.importRegister = function (id, key, value) {
    this.imports['CFComponent'+id] = {};
    this.imports['CFComponent'+id][key] = value;
  };

  CFcore.getImport = function (componentId, key) {
    return this.imports[componentId][key];
  }

  var componentRegister = function (id, component, parent) {
    extend(component, parent);

    function live (component) {
      component.init();
      component.initialize();
      component.run();
      component.afterRun();
    };

    if (CFcore.components[id]['component']) {
      // component loaded - it's child
      if (CFcore.components[id]['component'].allowChild) {
        var obj = new component;
        this.components[id]
          .component
          .children
          .push(obj);

        CFcore.components[id]['component']
          .childRegistered(obj);

      } else {
        // try to restore
        CFcore.components[id].component.restore();
      }
    } else {
      CFcore.components[id]['component'] = new component;
      // check if style loaded
      var styles = CFcore.components[id].styles;

      if (styles) {
        var length = styles.length;

        for (var i in styles) {
          CFcore
            .getComponent('Inserter')
            .insertStyle(id, styles[i], function () {
              if (i == length - 1) {
                live(CFcore.components[id]['component']);
              }
            });
        }
      } else {
        live(CFcore.components[id]['component']);
      }
    }
  }

  CFcore.consoleComponentRegister = function (id, component) {
    componentRegister(id, component, ConsoleComponent);
  };

  CFcore.componentRegister = function (id, component) {
    componentRegister(id, component, Component);
  };

  CFcore.registerComponentAlias = function (id, alias) {
    CFcore.componentAliases[alias] = id;
  };

  CFcore.getComponentAlias = function (alias) {
    return this.componentAliases[alias];
  }

  CFcore.getComContainer = function (id) {
    var alias = this.getComponentAlias(id);

    if (alias != undefined) {
      id = alias;
    }

    return this.components[id] || (this.components[id] = {});
  };

  CFcore.getComponent = function (id) {
    console.log("ID:" + id);
    // integer given, not core component
    var componentId = id;
    if (!isNaN(id)) {
      componentId = "CFComponent" + id;
    }

    if (!this.components[componentId]) {
      try {
        if (componentId.indexOf("CFComponent") == -1) {
          componentId = this.getComponentAlias(id);
        }

        return this.components[componentId]['component'];
      } catch (e) {
        throw Error("Component " + componentId + " not found");
      }
    }

    return this.components[componentId]['component'];
  };

  // Cloud fun console component
  var ConsoleComponent = function () {

  };

  ConsoleComponent.allowChild = false;

  ConsoleComponent.prototype.init = function () {

  };

  ConsoleComponent.prototype.run = function () {

  };

  ConsoleComponent.prototype.afterRun = function () {

  };

  ConsoleComponent.prototype.initialize = function () {

  };

  ConsoleComponent.prototype.stop = function () {

  };

  ConsoleComponent.prototype.stoped = function () {

  };

  ConsoleComponent.prototype.childRegistered = function (component) {
    component.init();
    component.initialize();
    component.run();
  };

  // Cloud fun component
  var Component = function () {
  };

  CFcore.common = {
    extend: extend,
    component: Component
  };
  Component.prototype.allowChild = false;
  Component.prototype.isDraggable = true;
  Component.prototype.isResizable = true;

  Component.prototype.children = [];
  Component.prototype.childRegistered = function (component) {
    component.init();
    component.initialize();
    component.run();
  };

  Component.prototype.init = function () {
    console.log('Component init');
  };

  Component.prototype.afterRun = function () {
      // attach container actions
      this.behaviours();
  };

  Component.prototype.run = function () {
    console.log('Component run');
  };

  Component.prototype.stop = function () {
    $(this.host).remove();

    if (this.children.length > 0) {
      // child component
      this.children.pop();
    } else {
      $('[data-id='+this.id+']').remove();
      CFcore.components[this.id]["component"] = null;

      if (this.trayItem) {
        this.trayItem.remove();
      }
    }
    this.stoped();
  };

  Component.prototype.behaviours = function () {
    var _this = this;
    var host = $(_this.host);

    if (this.isDraggable) {
      host.draggable({handle: '.CFCore-window-action'});
    }

    if (this.isResizable) {
      host.resizable();
    }

    host
      .find('.action-hide')
      .on('click', function () {
        _this.hide();
      });

    host
      .find('.action-maximize')
      .on('click', function () {
        _this.maximize();
      });

    host
      .find('.action-close')
      .on('click', function () {
        _this.close();
      });

    host
      .find('.action-minimize')
      .on('click', function () {
        _this.minimize();
      });
  };

  Component.prototype.hide = function () {
    console.log('Hide component');
    if (this.trayItem) {
      this.trayItem.hided();
    }

    this.host.fadeOut('slow');

    this.hided();
  };

  Component.prototype.close = function () {
    console.log('Close component');
    this.stop();
  };

  Component.prototype.minimize = function () {
    console.log('Minimize');

    var host = this.host;

    host.css('width', this._containerWidth);
    host.css('height', this._containerHeight );
    host.css('top', this._containerTop);
    host.css('left', this._containerLeft);
    host.find('.action-minimize').hide();
    host.find('.action-maximize').show();

    this.minimized();
  };

  Component.prototype.minimized = function () {

  };

  Component.prototype.restore = function () {
    console.log('Restore component');
    var desktop = CFcore.getComponent('desktop');
    var host = $(this.host);
    console.log(host);
    console.log("Restore");
    if (host.css('display') == 'none') {
      if (this.trayItem) {
        this.trayItem.restored();
      }

      this.host.fadeIn('slow');

      this.restored();
    }
  };

  Component.prototype.restored = function () {

  };

  Component.prototype.maximize = function () {
    console.log('Maximize component');
    var host = this.host;

    this._containerWidth = host.outerWidth();
    this._containerHeight = host.outerHeight();
    this._containerLeft = host.css('left');
    this._containerTop = host.css('top');

    host.css('width', '100%');
    host.css('height', '100%');
    host.css('top', 0);
    host.css('left', 0);
    host.css('margin-left', 0);
    host.css('margin-right', 0);
    host.find('.action-maximize').hide();
    host.find('.action-minimize').show();

    this.maximized();
  };

  Component.prototype.maximized = function  () {

  };

  Component.prototype.initialize = function () {

  };

  Component.prototype.hided = function () {

  };

  Component.prototype.stoped = function () {

  };

  CFcore.components['Http'] = {
    component: (function () {
      var start = function (options) {
        var Http = function (options) {
          var promise = new window.promise.Promise();
          
          var localOptions = {
            success: function (response) {
              promise.done(response);
            }
          };

          if (options.withCreds) {
            options.url += "&access-token=" + CFcore.accessToken;
          }
          options = Object.assign(localOptions, options);

          $.ajax(options);

          return promise;
        };

        return new Http(options);
      }
      return {
        get: function (options) {
          options.method = "get";

          return start(options);
        },
        post: function (options) {
          options.method = "post";

          return start(options);
        }
      };
    })()
  };

  CFcore.components['Preloader'] = {
    component: {
      container: document.getElementById("welcome-container"),
      preloader: document.querySelector('#welcome-container i'),
      hidePreloader: function () {
        this.preloader.style.display = 'none';
      },
      showPreloader: function () {
        this.preloader.style.display = 'block';
      },
      hideContainer: function () {
        this.container.style.display = 'none';
      },
      showContainer: function () {
        this.container.style.display = 'block';
      }
    }
  };

  CFcore.components.Inserter = (function () {
    const types = {
      2 : 'Style'
    };

    return {
      component: {
        register: function (options) {
          var insertFunc = this['register' + types[options.type]];

          insertFunc(options.id, options.src);
        },
        registerStyle: function (id, src) {
          var componentId = "CFComponent" + id;

          if (!CFcore.components[componentId]) {
            CFcore.components[componentId] = {
              styles: []
            };
          }

          if (CFcore.accessToken != null) {
            src += "&access-token=" + CFcore.accessToken;
          }
          CFcore.components[componentId].styles.push(src);
        },
        insert: function (options, callback) {
          var insertFunc = this['insert' + types[options.type]];

          insertFunc(options.id, options.src, callback);
        },
        insertComponent: function (id, urlParams) {
          var componentId = id;
          if (!isNaN(id)) {
            // integer given
            componentId = "CFComponent" + id;
          }

          var comContainer = CFcore.components[componentId];

          var insert = function () {
            var script = document.createElement('script');
            script.setAttribute('data-id', componentId);
            script.setAttribute('src', getComponentSrc(id) + urlParams);
            script.setAttribute('onload', 'CFcore.componentLoaded(this);');

            document.body.appendChild(script);
          }

          if (!comContainer || comContainer.allowChild) {
            insert();
          } else if (comContainer.component) {
            comContainer.component.restore();
          } else {
            insert();
          }
        },
        insertScript: function (id, src, callback) {

        },
        insertStyle: function (id, src, callback) {
          var style = document.createElement('link');
          style.onload = callback;
          style.setAttribute('data-id', id);
          style.setAttribute('href', src);
          style.setAttribute('rel', 'stylesheet');
          style.setAttribute('type', 'text/css');

          document.head.appendChild(style);
        },
        remove: function (id) {

        }
      }
    };
  })();

  CFcore.components['Loader'] = {
    component: {
      getFile: function (id, withCredentials, callback) {
        if (withCredentials) {
          id += "&access-token="+CFcore.accessToken
        }
        $.ajax({
          url: 'http://api.cloud-fun.loc/loader/load?id='+id,
          success: callback
        })
      }
    }
  };

  CFcore.components['Launcher'] = {
    component: {
      start: function (id, withCredentials, params) {
        console.log(id);
        var urlParams = '';
        params = JSON.stringify(params) || '';
        if (withCredentials) {
          urlParams = '&access-token=' + CFcore.accessToken;
        }
        if (params) {
          urlParams += "&params=" + params;
        }
        CFcore.getComponent('Inserter')
          .insertComponent(id, urlParams);
      }
    }
  };

  CFcore.accessToken = $.cookie("access_token");

})(window, document, window.CFcore || (window.CFcore = {}));