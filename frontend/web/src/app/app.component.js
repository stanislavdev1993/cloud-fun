(function (app) {
  'use strict'

  console.log(ng)
  app.AppComponent =
    ng.core.Component({
      selector: 'app-root',
      template: 'app-component',
      providers: [app.AutoloadService, app.ComponentInserter],
    })
      .Class({
        constructor: [
          ng.compiler.CompileMetadataResolver,
          ng.compiler.JitCompiler,
          ng.core.ViewContainerRef,
          ng.core.ComponentFactoryResolver,
          app.AutoloadService,
          app.ComponentInserter,
          function (
                    componentResolver,
                    compiler,
                    viewContainerRef,
                    componentFactoryResolver,
                    autoloadService,
                    componentInserter) {
            this.compiler = compiler;
            app.componentLoaded = new ng.core.EventEmitter
            this.viewContainerRef = viewContainerRef
            this.componentResolver = componentResolver;
            this.componentFactoryResolver = componentFactoryResolver;
            this.autoloadService = autoloadService
            this.componentInserter = componentInserter
            this.autoloads = []

          }],
        ngOnInit: function () {
          var _this = this

          app.componentLoaded.subscribe(function (component) {
            var l = _this.compiler
            console.log(l);
            // console.log('Main');
            // console.log(app.main);
            // console.log(window.bla);
            // ComponentFactoryBoundToModule
            console.log('Jit compiler');
            console.log(_this.componentResolver);
            var forever = _this.componentResolver
              .getHostComponentViewClass(app.AppComponent);
            // console.log('Forever');
            // console.log((forever));
            console.log(_this.componentFactoryResolver)
            // console.log(forever);
            var componentFactory = ng.core['ɵccf']('login',
              app.LoginComponent,
              (forever),
              [], [], [])
            // console.log(componentFactory)
            //
            // console.log(_this)
            _this.componentFactoryResolver['_factories']
              .set(app.LoginComponent, componentFactory)
            // console.log('Event component');
            // console.log(window);
            // console.log(component);
            // .set(app.LoginComponent, createComponentFactory());
            //getComponentViewClass
            var factory = _this.componentFactoryResolver
              .resolveComponentFactory(component)
            //
            var comp = _this.viewContainerRef
              .createComponent(factory)
          })

          _this.autoloadService.eventRecieved.subscribe(function () {
            console.log('Autoloads uploaded')
            // auto loads received, start insert in page

            console.log(_this.autoloads)
            _this.componentInserter.insertAll(_this.autoloads)
          })

          _this.autoloadService.get().then(function (autoloads) {
            _this.autoloads = autoloads
            _this.autoloadService.eventRecieved.emit()
          })
        }
      })
})(window.app || (window.app = {}))