(function (app, document) {
    "use strict";

    const types = {
        1: 'Script',
        2: 'Style',
        3: 'File',
        4: 'Component'
    };

    app.ComponentInserter = ng.core.Class({
        constructor: function () {
            this.document = document;

            if (typeof window.componentsInserted == "undefined") {
                window['componentsInserted'] = [];
            }
        },
        insertAll: function (components) {
            for (var i in components) {
                this.insert(components[i]);
            }
        },
        insert: function (component) {
            var insertFunc = 'insert' + types[component.type];
            // console.log(insertFunc);
            this[insertFunc](component.file_id, component.src);
            window['componentsInserted'].push(component.file_id);
        },
        insertScript: function (id, src) {
            console.log('insert script');
            var script = this.document.createElement('script');

            script.setAttribute('type', 'text/javascript');
            script.setAttribute('data-id', id);
            script.setAttribute('src', src);

            this.document.body.appendChild(script);
        },
        insertComponent: function (id, src) {
            // component insert script and styles together
            console.log('Insert component func');
            this.insertScript(id, src);
        },
        insertStyle: function (id, src) {
            console.log('insert Style');
            var style = document.createElement('link');

            style.setAttribute('data-id', id);
            style.setAttribute('href', src);
            style.setAttribute('rel', 'stylesheet');

            this.document.head.appendChild(style);
        }
    });
})(window.app || (window.app = {}), document);
