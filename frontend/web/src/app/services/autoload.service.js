(function (app) {
    "use strict";

    console.log('Autoload service loaded');
    app.AutoloadService = ng.core.Injectable(ng.http.Http)
        .Class({
            constructor: [ng.http.Http, function (http) {
                // injection http service
                this.http = http;
                this.eventRecieved = new ng.core.EventEmitter();
                this.apiUrl = 'http://api.cloud-fun.loc/autoload';
            }],
            get: function () {
                var _this = this;
                return this.http.get(this.apiUrl)
                    .toPromise()
                    .then(function (response) {
                        return response.json();
                    })
                    .catch(this.handleError);
            },
            handleError: function () {
                
            }
    });
})(window.app || (window.app = {}));