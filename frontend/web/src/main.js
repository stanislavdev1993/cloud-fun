(function(app) {
    "use strict";

    document.addEventListener('DOMContentLoaded', function() {
        ng.platformBrowserDynamic
            .platformBrowserDynamic()
            .bootstrapModule(app.AppModule).then(function (appModule) {
                console.log('Bootstrap');
                app.MainModule = appModule;
        });
    });
})(window.app || (window.app = {}));