<?php

namespace frontend\assets;

use common\models\Autoload;
use common\models\User;
use Yii;
use yii\web\AssetBundle;
use yii\web\NotFoundHttpException;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/core.css',
        'libs/jquery-ui/jquery-ui.min.css',
        'libs/tether-1.3.3/css/tether.css',
        'libs/bootstrap4/css/bootstrap.min.css',
        'libs/font-awesome-4.7.0/css/font-awesome.min.css'
    ];
    public $js = [
        'libs/promise.min.js',
        'libs/jquery/jquery-3.2.1.min.js',
        'libs/jquery-ui/jquery-ui.js',
        'libs/jquery.upload/jquery.upload.js',
        'libs/jquery/jquery.cookie.js',
        'libs/tether-1.3.3/js/tether.min.js',
        'libs/bootstrap4/js/bootstrap.min.js',
        'js/CFcore.js'
    ];

    public function init()
    {
        parent::init();

        $accessToken = '';
        if (isset($_COOKIE['access_token'])) {
            $user = User::findIdentityByAccessToken($_COOKIE['access_token']);

            if ($user !== null) {
                Yii::$app->user->login($user);
                $accessToken = '&access-token=' . $user->access_token;
            }
        }

        $autoloads = Autoload::find()
            ->select('file_id')
            ->with('file')
            ->where(['user_id' => 0])
            ->andWhere(['for_guest' => Yii::$app->user->isGuest])
            ->all();

        if (!Yii::$app->user->isGuest) {
            $userId = Yii::$app->user->getId();

            $models = Autoload::find()
                ->select('file_id')
                ->where(['user_id' => $userId])
                ->all();

            $autoloads = $autoloads + $models;
        }

        foreach ($autoloads as $autoload) {
            $this->js[] = [$autoload->src . $accessToken,
                'data-id' => 'CFComponent' . $autoload->file_id,
                'onload' => 'CFcore.componentLoaded(this);'
            ];
        }
    }
}
