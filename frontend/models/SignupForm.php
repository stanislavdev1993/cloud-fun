<?php
namespace frontend\models;

use common\helpers\FileHelper;
use common\models\Group;
use common\models\UserGroup;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_confirm;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['password_confirm', 'required'],
            ['password_confirm', 'compare', 'compareAttribute' => 'password']
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->setAccessToken();
        $user->dir = substr(md5($this->username . time()), 0, 10);

        if ($user->save() && FileHelper::createUserFolder($user->dir)) {
            return $this->userToRegistered($user->id) ? $user : null;
        }

        return null;
    }

    protected function userToRegistered($id)
    {
        $registered = Group::findOne(['title' => 'Registered']);

        $userGroup = new UserGroup();
        $userGroup->user_id = $id;
        $userGroup->group_id = $registered->id;

        return $userGroup->save();
    }
}
