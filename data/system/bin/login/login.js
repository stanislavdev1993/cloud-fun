/* CFcore login component */
(function (CFComponent2) {
  "use strict";

  // loaded callback
  CFComponent2['callback'] = function (host, componentId) {
    console.log('Component host');
    console.log(host);
    var CFLogin = function () {
      var template = CFcore.getImport(componentId, 'template');
      var _this = this;
      this.isResizable = false;
      // this.allowChild = true;

      this.init = function () {
        host.innerHTML = template;
        CFcore.appsContainer.appendChild(host);
      };

      var singInForm = null;
      var singUpForm = null;

      this.signUpClick = function () {
        host.addClass('animation');

        var timeout = setTimeout(function () {
          singInForm.css("display", "none");
          singUpForm.css("display", "block");
          host.removeClass('animation');

          clearTimeout(timeout);
        }, 405);
      };

      this.signInClick = function () {
        host.addClass('animation');

        var timeout = setTimeout(function () {
          singInForm.css("display", "block");
          singUpForm.css("display", "none");
          host.removeClass('animation');

          clearTimeout(timeout);
        }, 405);
      };

      this.initialize = function () {
        var preloader = CFcore.getComponent('Preloader');
        preloader.hidePreloader();

        host = $(host);
        singUpForm = host.find('#sign-up-form');
        singInForm = host.find('#sign-in-form');

        host.find('.sign-up').on('click', this.signUpClick);
        host.find('.sign-in').on('click', this.signInClick);
        singInForm.on('submit', this.submitSignIn);
        singUpForm.on('submit', this.submitSignUp);
      };

      var signinUrl = 'http://api.cloud-fun.loc/auth/sign-in';
      var signupUrl = 'http://api.cloud-fun.loc/auth/sign-up';

      this.submitSignIn = function () {
        var form = host.find('#sign-in-form');
        var formData = new FormData(form[0]);

        $.ajax({
          url: signinUrl,
          method: "POST",
          data: formData,
          processData: false,
          contentType: false,
          success: function (response) {
            if (response.status == 1) {
              // logined
              CFcore.accessToken = response.access_token;
              document.cookie = 'access_token='+CFcore.accessToken;
              _this.logined();
            }
          }
        });
      };

      this.submitSignUp = function () {
        console.log('Submit sign up');
        var form = host.find('#sign-up-form');
        var formData = new FormData(form[0]);

        $.ajax({
          url: signupUrl,
          method: "POST",
          data: formData,
          processData: false,
          contentType: false,
          success: function (response) {
            console.log('Sign up');
            console.log(response);

            if (response.status == 1) {
              CFcore.accessToken = response.access_token;
              document.cookie = 'access_token='+CFcore.accessToken;
              _this.logined();
            }
          }
        });
      };

      this.logined = function () {
        // Launch
        console.log('Logined');
        var finder = CFcore.getComponent('finder');
        finder.hide();
        // start desktop
        CFcore.getComponent('Launcher').start('storage', true);
        //start finder
        CFcore.getComponent('Launcher').start('desktop', true);
        CFComponent2.component.stop();
      };

      this.registered = function () {

      };
    };

    CFcore.componentRegister(componentId, CFLogin);
  };

})(window.CFcore.components['CFComponent2'] || (window.CFcore.components['CFComponent2'] = {}))