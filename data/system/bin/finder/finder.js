// CF core component finder
(function (FinderComponent) {
  console.log('Bla');
  // loaded callback
  console.log(FinderComponent);
  FinderComponent['callback'] = function (host, componentId) {
    var CFFinder = function () {
      var template = null;
      var _this = this;
      this.isResizable = false;

      this.init = function () {
        template = CFcore.imports[componentId]['template'];
        host.innerHTML = template;
      };
      this.show = function () {
        console.log('Show');
        host.find('.search').hide();
        host.find('.main').fadeIn('slow');
      };

      this.run = function () {
        CFcore.appsContainer.appendChild(host);
        host = $(host);

        if ($.cookie('access_token')) {
          _this.hide();
        }
      };

      this.hide = function () {
        console.log('Hide');
        var searchIcon = host.find('.search')
        var finder = host.find('.main');

        searchIcon.css('right', 5);
        searchIcon.css('top', 0);
        finder.hide();
        searchIcon.show();
      }
    };

    CFcore.componentRegister(componentId, CFFinder);
  };
})(window.CFcore.components['CFComponent4'] || (window.CFcore.components['CFComponent4'] = {}));