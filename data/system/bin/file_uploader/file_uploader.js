(function (FileUploaderContainer) {
  FileUploaderContainer["callback"] = function (host, componentId) {
    var FileUploader = function () {
      var _this = this;
      this.isResizable = false;
      var api = "http://api.cloud-fun.loc/file/upload?access-token="+CFcore.accessToken;
      this.path = CFcore.getParams(componentId).path;
      var chunkSize = 10 * 1024 * 1024;

      var trayItem = document.createElement("li");

      trayItem.innerText = "Uploader";
      this.trayItem = CFcore.get("desktop").components.tray.addItem({
        element: trayItem
      }, _this);

      this.init = function () {
        host.innerHTML = CFcore.getImport(componentId, 'template');
        CFcore.get("desktop")
          .getMainArea()
          .append(host);
        _this.host = $(host);

        var fileInput = _this.host.find('.file-input');
        _this.host.find('.upload').on('click', function () {
          fileInput.click();
        });
        fileInput.fileupload({
          url: api,
          formData: {
            path: _this.path
          },
          replaceFileInput: false,
          maxChunkSize: chunkSize, // 10 MB
          add: function (e, data) {
            _this.host.find('.progress').show();
            data.submit();
            // fix once upload
            return false;
          },
          progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            var progressBar = _this.host.find('.progress-bar');
            progressBar.css(
              'width',
              progress + '%'
            );
            progressBar.text(progress + '%');
          }
        }).bind('fileuploaddone', function (e, data) {
          var response = data.response();

          console.log("Download");
          $.each(response.result.files, function (index, file) {
            var desktop = CFcore.get("desktop");
            var shortcuts = desktop.components.shortcuts;
            var icon_url;

            if (!file.component_id) {
              icon_url = "http://cloud-fun.loc/img/unknown-icon.png";
            } else {
              icon_url = "http://api.cloud-fun.loc/component/icon?componentId="
                + file.component_id
                + "&access-token="
                + CFcore.accessToken;
            }

            // update storage
            var http = CFcore.get("Http");
            var storage = CFcore.get("storage");

            shortcuts.renderItem({
              icon_url: icon_url,
              title: file.name,
              launch: file.component_id,
              params: {
                url: file.url,
                file_id: file.file_id
              }
            });

            storage.update("desktop", shortcuts.getItemsForSave()).then(function (response) {
              console.log(response);
              console.log("Updated");
            });
          });

          _this.host.find('.progress').hide();
        });
      }
    };

    CFcore.componentRegister(componentId, FileUploader);
  };
})(CFcore.getComContainer('file_uploader'));