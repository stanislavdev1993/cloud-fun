/* CFcore login component without jquery */
(function (Desktop) {
  "use strict";

  var About = function (host) {
    this.host = $(host).find('.about');
    console.log("About constructor");
    var _this = this;
    this.trayItem = new TrayItem({
      element: host.querySelector(".about-tray-item")
    });

    this.close = function () {
      _this.host.fadeOut('slow');
    };

    this.run = function () {
      _this.trayItem.restored();
      _this.host.fadeIn('slow');
      _this.afterRun();
    };
  };
  CFcore.common.extend(About, CFcore.common.component);

  var Shortcuts = function (container, value, host) {
    var _this = this;
    _this.contextMenu = new FileContextMenu(host);
    _this.container = container;
    _this.items = [];

    value.items.forEach(function (item) {
      _this.renderItem(item);
    });

    window.onbeforeunload = function () {
      var storage = CFcore.get("storage");
      storage.update("desktop",
        CFcore.get("desktop").components
        .shortcuts
        .getItemsForSave()
      );

      return null;
    };

  };
  Shortcuts.prototype.getItemsForSave = function () {
    var result = [];

    $.each(this.items, function (index, item) {
      result.push({
        icon_url: item.icon_url,
        launch: item.launch,
        title: item.title,
        params: {
          file_id: item.params.file_id,
          url: item.params.url
        },
        x: item.x,
        y: item.y
      });
    });

    return result;
  };

  Shortcuts.prototype.draggable = function (desktopItem) {
    var element = $(desktopItem.getElement());
    element.draggable({
      handle: ".icon",
      stop: function (event, ui) {
        desktopItem.x = element.css("left");
        desktopItem.y = element.css("top");
      }
    });
  };

  Shortcuts.prototype.renameble = function (desktopItem) {
    console.log("Desktop item editable");
    var
      $element = $(desktopItem.getElement()),
      $text = $element.find('.text');

    var currentText = $text.text();

    $text.on("blur", function (e) {
      console.log("Blur");
      if ($(this).text().length == 0) {
        $text.text(desktopItem.title);
      }
    });

    $text.on("keyup", function (e) {
      var thisText = $(this).text();
      if (currentText != thisText && thisText.length > 0) {
        console.log("Changed");
        currentText = thisText;
        desktopItem.changeTitle(currentText);
      } else {
        console.log("Not changed");
      }
    });
  };

  Shortcuts.prototype.unselectAll = function (e) {
    this.items.forEach(function (item) {
      $(item.getElement()).removeClass("item-selected");
    });
  };

  Shortcuts.prototype.select = function (e) {
    var item = this;
    var element = $(this.getElement());

    var hasClass = element.hasClass("item-selected");

    if (hasClass) {
      element.removeClass("item-selected");
    } else {
      element.addClass("item-selected");
    }

    e.stopPropagation();
  };

  Shortcuts.prototype.launch = function (e) {
    var item = this;
    var launcher = CFcore.get("Launcher");

    // if unknown file then try to download
    if (!item.launch) {
      window.location.href = item.params.url + "&access-token=" + CFcore.accessToken
    } else {
      launcher.start(item.launch, true, item.params);
    }
  };

  Shortcuts.prototype.renderItem = function (data) {
    var
      container = document.createElement("div"),
      icon = document.createElement('span'),
      title = document.createElement('span');

    container.className = "desktop-item";
    container.setAttribute("data-launch", data.launch);
    container.setAttribute("data-params", JSON.stringify(data.params));
    container.appendChild(icon);
    container.appendChild(title);

    if (!data.x) {
      data.x = "0px";
    }

    if (!data.y) {
      data.y = "0px";
    }

    container.style.left = data.x;
    container.style.top = data.y;
    icon.className = "icon";
    icon.style.backgroundImage = "url("+ data.icon_url + ")";
    icon.style.backgroundSize = "contain";
    title.className = "text";
    title.innerText = data.title;
    title.setAttribute("contenteditable", "");
    title.setAttribute("spellcheck", false);

    var item = new DesktopItem(container, data);
    console.log(this.contextMenu);

    this.draggable(item);
    this.renameble(item);
    this.items.push(item);
    var element = item.getElement();
    // desktop item events
    element.addEventListener("dblclick", this.launch.bind(item));
    element.addEventListener("click", this.select.bind(item));
    title.onclick = function (e) {e.stopPropagation();};

    element.addEventListener("contextmenu",
      this.contextMenu
      .contextMenuEvent
      .bind(item)
    );
    this.container.appendChild(element);
  };

  var Tray = function (container) {
    this.container = $(container).find(".tray-menu");
    this.addItem = function (options, component) {
      var item = new TrayItem(options);
      item.element.on("click", function () {
        component.restore();
      });
      this.container.append(item.element);

      return item;
    };
  };

  var TrayItem = function (options) {
    this.element = $(options.element);
  };

  TrayItem.prototype.hided = function () {
    this.element.addClass("hidden-item");
  };

  TrayItem.prototype.restored = function () {
    this.element.removeClass("hidden-item");
  };

  TrayItem.prototype.remove = function () {
    this.element.remove();
  };

  var DesktopItem = function (element, data) {
    var el = element;

    this.icon_url = data.icon_url;
    this.launch = data.launch;
    this.params = data.params;
    this.title = data.title;
    this.x = data.x;
    this.y = data.y;

    this.getElement = function () {
      return el;
    };
  };

  DesktopItem.prototype.changeTitle = function (title) {
    this.title = title;
    console.log(title);
    console.log("change title");
    var shortcutsRequest = CFcore.get("storage");

    shortcutsRequest.update("desktop",
      CFcore.get("desktop").components
        .shortcuts
        .getItemsForSave()
    ).then(function (shortcuts) {
      console.log(shortcuts);
    });

    console.log(shortcutsRequest);
  };

  var DesktopContextMenu = function (host) {
    var element = $(host.querySelector(".context-menu"));

    var hideFileMenu = function () {
      CFcore.get("desktop")
        .components
        .shortcuts
        .contextMenu
        .hide();
    };

    this.hide = function () {
      // hide file memu if opened
      hideFileMenu();
      element.fadeOut("slow");
    };

    this.contextmenuEvent = function (e) {
      hideFileMenu();
      element.css("left", e.clientX - 15 + "px");
      element.css("top", e.clientY + "px");
      element.fadeIn("slow");

      e.preventDefault();
    };
  };
  CFcore.common.extend(DesktopContextMenu, CFcore.common.component);

  var FileContextMenu = function (host) {
    this.host = host;
    var _this = this;

    this.getShortCuts = function () {
      return CFcore.get("desktop").components.shortcuts;
    };

    this.contextMenuEvent = function (e) {
      console.log("Context menu");
      _this.currentItem = this;
      _this.host.css("left", e.clientX - 15 + "px");
      _this.host.css("top", e.clientY + "px");
      _this.host.fadeIn("slow");

      e.preventDefault();
      e.stopPropagation();
    };
  };
  CFcore.common.extend(FileContextMenu, CFcore.common.component);
  FileContextMenu.prototype.open = function () {
    var shortcuts = this.getShortCuts();

    shortcuts.launch.call(this.currentItem);
    this.hide();
  };

  FileContextMenu.prototype.cut = function () {
    this.currentItem.cutted = true;
    var item = this.currentItem.getElement();
    $(item).addClass("cutted");
  };

  FileContextMenu.prototype.copy = function () {
    console.log("Copy");
    console.log(this.currentItem);
  };

  FileContextMenu.prototype.rename = function () {
    var element = $(this.currentItem.getElement());
    var textElement = element.find(".text");
    textElement.attr("contenteditable", "");
    textElement.focus();
  };

  FileContextMenu.prototype.delete = function () {

  };

  FileContextMenu.prototype.send = function () {

  };

  FileContextMenu.prototype.properties = function () {

  };

  var Background = function (host) {
    this.isResizable = false;
    this.id = 'CFComponentBackground';
    this.host = $(host).find('#background');
    var _this = this;

    var trayItem = document.createElement("li");

    trayItem.innerText = "Background";

    var desktop = CFcore.get("desktop");

    // init window behaviours
    _this.behaviours();

    this.run = function () {
      _this.trayItem = CFcore.get("desktop")
        .components
        .tray
        .addItem({
        element: trayItem
      }, _this);

      _this.host.fadeIn('slow');
    };

    this.stop = function () {
      _this.trayItem.remove();
      _this.host.fadeOut('slow', function () {
        _this.minimize();
      });
    };
  };

  CFcore.common.extend(Background, CFcore.common.component);

  Desktop['callback'] = function (host, componentId) {
    var Desktop = function () {
      this.isResizable = false;
      this.isDraggable = false;

      var _this = this,
        api = "http://api.cloud-fun.loc/",
        template = CFcore.getImport(componentId, "template"),
        mainArea = null,
        components = {};

      this.components = components;

      this.getMainArea = function () {
        return mainArea;
      };
      this.init = function () {
        var shortcutsRequest = CFcore.get("storage").get("desktop");

        shortcutsRequest.then(function (shortcuts) {
          // retrive shortcuts
          _this.host.innerHTML = template;
          mainArea = _this.host.querySelector("#main-area");
          var contextMenu = components
            .desktopContextMenu = new DesktopContextMenu(_this.host);

          mainArea.addEventListener("contextmenu", contextMenu
            .contextmenuEvent
            .bind(contextMenu)
          );

          components.about = new About(_this.host);
          components.tray = new Tray(_this.host.querySelector(".tray"));
          components.background = new Background(host);

          _this.host.addEventListener("click",
            contextMenu
            .hide
            .bind(contextMenu)
          );

          components.shortcuts = new Shortcuts(mainArea,
            JSON.parse(shortcuts.value),
            $(host).find(".file-context-menu")
          );
          _this.host.addEventListener("click",
            components.shortcuts
            .unselectAll
            .bind(components.shortcuts)
          );

          CFcore.get("Preloader").hideContainer();
        });
      };

      this.run = function () {
        CFcore
          .appsContainer
          .appendChild(_this.host);
      };
      
      this.fullScreen = function () {
        if (!document.fullscreenElement &&    // alternative standard method
          !document.mozFullScreenElement && !document.webkitFullscreenElement) {  // current working methods
          if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
          } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
          } else if (document.documentElement.webkitRequestFullscreen) {
            document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
          }
        } else {
          if (document.cancelFullScreen) {
            document.cancelFullScreen();
          } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
          } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
          }
        }
      }

      this.logout = function () {
        $.removeCookie('access_token');
        location.reload();

      };
    };

    CFcore.componentRegister(componentId, Desktop);
  }

})(CFcore.getComContainer('desktop'))