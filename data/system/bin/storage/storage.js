(function (Storage) {
    Storage['callback'] = function (host, componentId) {
      // storage console component
      var CFStorage = function () {
        var api = 'http://api.cloud-fun.loc/storage/'

        this.set = function (key, value) {
        }

        this.update = function (key, value) {
          var http = CFcore.get("Http");

          return http.post({
            url: api + "update?access-token="+CFcore.accessToken,
            data: {
              key: key,
              value: value
            }
          });
        }

        this.get = function (key) {
          var http = CFcore.get("Http");

          return http.get({
            url: api
              + "get?key=" + key,
            withCreds: true
          });
        }
      };

      CFcore.consoleComponentRegister(componentId, CFStorage);
    }
})(CFcore.getComContainer('storage'))